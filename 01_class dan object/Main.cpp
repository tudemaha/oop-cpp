#include <iostream>
#include <string>

using namespace std;

// deklarasi class dilakukan sebelum main function
class Mahasiswa {
    public:
        string nama;
        string NIM;
        string jurusan;
        double IPK;
};

int main() {

    Mahasiswa data1;
    data1.nama = "ucup";
    data1.NIM = "34553";
    data1.jurusan = "Teknik mendubbing";
    data1.IPK = 7;

    Mahasiswa data2;
    data2.nama = "otong";
    data2.NIM = "83450";
    data2.jurusan = "Profesi programmer";
    data2.IPK = 4;

    cout << "nama dari data1: " << data1.nama << endl;
    cout << "NIM dari data1: " << data1.NIM << endl;
    cout << "Jurusan dari data1: " << data1.jurusan << endl;
    cout << "IPK dari data1: " << data1.IPK << endl << endl;

    cout << "nama dari data2: " << data2.nama << endl;
    cout << "NIM dari data2: " << data2.NIM << endl;
    cout << "jurusan dari data2: " << data2.jurusan << endl;
    cout << "IPK dari data2: " << data2.IPK << endl;
    

    return 0;
}
