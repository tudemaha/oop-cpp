#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>

using namespace std;

class Mahasiswa {
    public:
    string nama;
    string nim;
    string jurusan;

    Mahasiswa(string nama, string nim, string jurusan) {
        Mahasiswa::nama = nama;
        Mahasiswa::nim = nim;
        Mahasiswa::jurusan = jurusan;
    }

    string stringify() {
        string concat = nama + " " + nim + " " + jurusan;
        concat.erase(remove(concat.begin(), concat.end(), '\n'), concat.end());
        return concat;
    }

};

class DBase {
    public:
        ifstream in;
        ofstream out;
        string fileName;

        DBase(const char* fileName) {
            DBase::fileName = fileName;
        }

        void save(Mahasiswa data) {
            cout << data.nama << endl;
            cout << data.nim << endl;
            cout << data.jurusan << endl;

            DBase::out.open(DBase::fileName, ios::out);
            DBase::out << data.stringify() << endl;
            DBase::out.close();
        }

        void showall() {
            DBase::in.open(DBase::fileName, ios::in);
            string nama, nim, jurusan;
            string line, text;
            int index = 1;

            while(!DBase::in.eof()) {
                DBase::in >> nama;
                DBase::in >> nim;
                DBase::in >> jurusan;

                if(nama == "" || nim == "" || jurusan == "") {
                    continue;
                }

                cout << index++ << ". ";
                cout << nama << "\t";
                cout << nim << "\t";
                cout << jurusan << endl;
            }

            DBase::in.close();
        }
};

int main() {
    string nama, nim, jurusan;

    // creating database object
    DBase database = DBase("database.txt");

    cout << "===== DATA LAMA =====" << endl;
    database.showall();

    // input user
    cout << endl << "===== MASUKKAN DATA MAHASISWA BARU =====" << endl;
    cout << "Nama: ";
    cin >> nama;

    cout << "NIM: ";
    cin >> nim;

    cout << "Jurusan: ";
    cin >> jurusan;


    // creating mahasiswa object
    Mahasiswa dataMahasiswa = Mahasiswa(nama, nim, jurusan);

    // save to database
    database.save(dataMahasiswa);

    // show all data in database
    cout << endl << "===== DATA BARU =====" << endl;
    database.showall();

    return 0;
}