#include <iostream>
#include <string>

using namespace std;

// tanpa constructor
class Polos {
    public:
        string dataString;
        int dataInt;
};

// dengan constructor
class Mahasiswa {
    // harus public
    public:
        string nama;
        string NIM;
        string jurusan;
        double IPK;

        // dipanggil pertama kali (constructor)
        // Mahasiswa() {
        //     cout << "Ini adalah constructor" << endl;
        // }

        //constructor dengan parameter
        Mahasiswa(string inputNama, string inputNIM, string inputJurusan, double inputIPK) {
            Mahasiswa::nama = inputNama;
            Mahasiswa::NIM = inputNIM;
            Mahasiswa::jurusan = inputJurusan;
            Mahasiswa::IPK = inputIPK;

            // tambahkan ini biar tidak mengulang-ulang
            cout << Mahasiswa::nama << endl;
            cout << Mahasiswa::NIM << endl;
            cout << Mahasiswa::jurusan << endl;
            cout << Mahasiswa::IPK << endl;
        }
};

int main() {

    Mahasiswa mahasiswa1 = Mahasiswa("ucup", "123", "Memasak", 3.5);
    Mahasiswa mahasiswa2 = Mahasiswa("otong", "124", "Sipil", 3.8);

    // Polos objectPolos;
    // objectPolos.dataString = "Halo";
    // objectPolos.dataInt = 0;

    // cout << objectPolos.dataString << endl;
    // cout << objectPolos.dataInt << endl;
    return 0;
}