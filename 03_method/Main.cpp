#include <iostream>
#include <string>

using namespace std;

class Mahasiswa {
    public:
        string nama;
        double IPK;

        Mahasiswa(string nama, double IPK) {
            Mahasiswa::nama = nama;
            Mahasiswa::IPK = IPK;
        }

        // method di dalam class
        // tanpa parameter, tanpa return
        void tampilkanData() {
            cout << "Nama saya " << nama << ", IPK saya ";
            cout << IPK << endl;
        }

        // dengan parameter, tanpa return
        void ubahNama(const char *namaBaru) {    // string lebih lambat, ganti dengan const char *
            Mahasiswa::nama = namaBaru;
        }

        // tanpa parameter, dengan return
        string getNama() {
            return Mahasiswa::nama;
        }

        double getIPK() {
            return IPK;
        }

        // dengan parameter, dengan return
        double katrolIPK(const double &tambahNilai) {   // harus const untuk pakai reference
            return Mahasiswa::IPK + tambahNilai;
        }    
};

// // method di luar kelas (tapi tidak spesifik untuk class Mahsiswa)
// void tampilkanData(Mahasiswa data) {
//     cout << data.nama << endl;
//     cout << data.IPK << endl;
// }

int main() {
    Mahasiswa mahasiswa1 = Mahasiswa("ucup", 2.5);
    Mahasiswa mahasiswa2 = Mahasiswa("otong", 4.0);
    mahasiswa1.tampilkanData();
    mahasiswa2.tampilkanData();

    mahasiswa1.ubahNama("mario");
    mahasiswa2.tampilkanData();

    string dataNama = mahasiswa1.getNama();
    cout << "Data nama:" << dataNama << endl;
    cout << "Data IPK: " << mahasiswa1.getIPK() << endl;

    cout << "Nilai katrol " << mahasiswa2.katrolIPK(-1.0) << endl;
    return 0;
}